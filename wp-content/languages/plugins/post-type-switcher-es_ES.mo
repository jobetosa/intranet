��          �            h     i  M   �     �     �     �     �     �  	         
  
        (     C  /   H  1   x  )  �     �  S   �     D     M     T     f  
   t          �     �      �     �  /   �  4                                   	       
                                         &mdash; No Change &mdash; Allow switching of a post type while editing a post (in post publish section) Cancel Edit John James Jacoby Missing data. OK Post Type Post Type Switcher Post Type: Sorry, you cannot do this. Type https://profiles.wordpress.org/johnjamesjacoby/ https://wordpress.org/plugins/post-type-switcher/ PO-Revision-Date: 2020-02-17 07:47:53+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Post Type Switcher - Stable (latest release)
 &mdash; Sin cambios &mdash; Permite el cambio de tipo de contenido mientras lo editas (en la sección Publicar) Cancelar Editar John James Jacoby No hay datos. De acuerdo Tipo de contenido Post Type Switcher Tipo de contenido: Lo siento, no puedes hacer esto. Tipo https://profiles.wordpress.org/johnjamesjacoby/ https://es.wordpress.org/plugins/post-type-switcher/ 