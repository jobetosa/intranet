<?php
/**
 * The template for displaying singular post-types: posts, pages and user-defined custom post types.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php
while ( have_posts() ) : the_post();
	?>

<main <?php post_class( 'site-main' ); ?> role="main">
	<?php if ( apply_filters( 'hello_elementor_page_title', true ) ) : ?>
	
	<?php endif; ?>
	<div class="page-content">
	    <?php if(get_post_type()=='noticias_minambiente'):?>
        <div class="page-content" wfd-id="53">
				<div data-elementor-type="wp-post" data-elementor-id="2822" class="elementor elementor-2822" data-elementor-settings="[]" wfd-id="55">
			<div class="elementor-inner" wfd-id="56">
				<div class="elementor-section-wrap" wfd-id="57">
							<section class="elementor-element elementor-element-906a10b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="906a10b" data-element_type="section" wfd-id="213">
						<div class="elementor-container elementor-column-gap-default" wfd-id="214">
				<div class="elementor-row" wfd-id="215">
				<div class="elementor-element elementor-element-d306bbc elementor-column elementor-col-100 elementor-top-column" data-id="d306bbc" data-element_type="column" wfd-id="216">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="217">
					<div class="elementor-widget-wrap" wfd-id="218">
				<div class="elementor-element elementor-element-9d7fadb elementor-widget elementor-widget-image" data-id="9d7fadb" data-element_type="widget" data-widget_type="image.default" wfd-id="219">
				<div class="elementor-widget-container" wfd-id="220">
					<div class="elementor-image" wfd-id="221">
										<?php the_post_thumbnail('full'); ?>											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-9e1db27 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="9e1db27" data-element_type="section" wfd-id="197">
						<div class="elementor-container elementor-column-gap-default" wfd-id="198">
				<div class="elementor-row" wfd-id="199">
				<div class="elementor-element elementor-element-fa58f5e elementor-column elementor-col-50 elementor-top-column" data-id="fa58f5e" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="206">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="207">
					<div class="elementor-widget-wrap" wfd-id="208">
				<div class="elementor-element elementor-element-cee9141 elementor-widget elementor-widget-spacer" data-id="cee9141" data-element_type="widget" data-widget_type="spacer.default" wfd-id="209">
				<div class="elementor-widget-container" wfd-id="210">
					<div class="elementor-spacer" wfd-id="211">
			<div class="elementor-spacer-inner" wfd-id="212"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-597c242 elementor-column elementor-col-50 elementor-top-column" data-id="597c242" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="200">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="201">
					<div class="elementor-widget-wrap" wfd-id="202">
				<div class="elementor-element elementor-element-a31df63 elementor-widget elementor-widget-text-editor" data-id="a31df63" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="203">
				<div class="elementor-widget-container" wfd-id="204">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="205"><p><?php the_title(); ?></p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-867426d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="867426d" data-element_type="section" wfd-id="162">
						<div class="elementor-container elementor-column-gap-default" wfd-id="163">
				<div class="elementor-row" wfd-id="164">
				<div class="elementor-element elementor-element-75bdc3c elementor-column elementor-col-25 elementor-top-column" data-id="75bdc3c" data-element_type="column" wfd-id="194">
			<div class="elementor-column-wrap" wfd-id="195">
					<div class="elementor-widget-wrap" wfd-id="196">
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-b73ec36 elementor-column elementor-col-25 elementor-top-column" data-id="b73ec36" data-element_type="column" wfd-id="175">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="176">
					<div class="elementor-widget-wrap" wfd-id="177">
				<div class="elementor-element elementor-element-bb1de12 elementor-absolute elementor-widget elementor-widget-spacer" data-id="bb1de12" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="spacer.default" wfd-id="182">
				<div class="elementor-widget-container" wfd-id="183">
					<div class="elementor-spacer" wfd-id="184">
			<div class="elementor-spacer-inner" wfd-id="185"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-8eef186 elementor-absolute elementor-widget elementor-widget-spacer" data-id="8eef186" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="spacer.default" wfd-id="178">
				<div class="elementor-widget-container" wfd-id="179">
					<div class="elementor-spacer" wfd-id="180">
			<div class="elementor-spacer-inner" wfd-id="181"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-7ffb217 elementor-column elementor-col-25 elementor-top-column" data-id="7ffb217" data-element_type="column" wfd-id="168">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="169">
					<div class="elementor-widget-wrap" wfd-id="170">
				<div class="elementor-element elementor-element-e62ef4b elementor-absolute elementor-widget elementor-widget-spacer" data-id="e62ef4b" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="spacer.default" wfd-id="171">
				<div class="elementor-widget-container" wfd-id="172">
					<div class="elementor-spacer" wfd-id="173">
			<div class="elementor-spacer-inner" wfd-id="174"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-914cdcb elementor-column elementor-col-25 elementor-top-column" data-id="914cdcb" data-element_type="column" wfd-id="165">
			<div class="elementor-column-wrap" wfd-id="166">
					<div class="elementor-widget-wrap" wfd-id="167">
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-334cc68 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="334cc68" data-element_type="section" wfd-id="58">
						<div class="elementor-container elementor-column-gap-default" wfd-id="59">
				<div class="elementor-row" wfd-id="60">
				<div class="elementor-element elementor-element-edb2b10 elementor-column elementor-col-50 elementor-top-column" data-id="edb2b10" data-element_type="column" wfd-id="147">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="148">
					<div class="elementor-widget-wrap" wfd-id="149">
				<div class="elementor-element elementor-element-b39ca31 elementor-widget elementor-widget-text-editor" data-id="b39ca31" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="159">
				<div class="elementor-widget-container" wfd-id="160">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="161"><p>Publicado por: <?php the_author(); ?> Fecha: <?php the_date(); ?></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-16f1ade elementor-widget elementor-widget-text-editor" data-id="16f1ade" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="156">
				<div class="elementor-widget-container" wfd-id="157">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="158"><p><?php the_content(); ?></p></div>
				</div>
				</div>
				
				<div class="elementor-element elementor-element-48ab0f4 elementor-widget elementor-widget-image" data-id="48ab0f4" data-element_type="widget" data-widget_type="image.default" wfd-id="150">
				<div class="elementor-widget-container" wfd-id="151">
					<div class="elementor-image" wfd-id="152">
										<img width="800" height="453" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-1024x580.png" class="attachment-large size-large" alt="" srcset="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-1024x580.png 1024w, http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-300x170.png 300w, http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-768x435.png 768w, http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande.png 1858w" sizes="(max-width: 800px) 100vw, 800px">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-9e92613 elementor-column elementor-col-50 elementor-top-column" data-id="9e92613" data-element_type="column" wfd-id="61">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="62">
					<div class="elementor-widget-wrap" wfd-id="63">
				<section class="elementor-element elementor-element-cf0aaee elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="cf0aaee" data-element_type="section" wfd-id="133">
						<div class="elementor-container elementor-column-gap-default" wfd-id="134">
				<div class="elementor-row" wfd-id="135">
				<div class="elementor-element elementor-element-163befc elementor-column elementor-col-50 elementor-inner-column" data-id="163befc" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="141">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="142">
					<div class="elementor-widget-wrap" wfd-id="143">
				<div class="elementor-element elementor-element-3f5ec5c elementor-widget elementor-widget-image" data-id="3f5ec5c" data-element_type="widget" data-widget_type="image.default" wfd-id="144">
				<div class="elementor-widget-container" wfd-id="145">
					<div class="elementor-image" wfd-id="146">
										<img width="47" height="42" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/11/ico-consultasfrecuentes.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-1f45746 elementor-column elementor-col-50 elementor-inner-column" data-id="1f45746" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="136">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="137">
					<div class="elementor-widget-wrap" wfd-id="138">
				<div class="elementor-element elementor-element-b711472 elementor-widget elementor-widget-heading" data-id="b711472" data-element_type="widget" data-widget_type="heading.default" wfd-id="139">
				<div class="elementor-widget-container" wfd-id="140">
			<h2 class="elementor-heading-title elementor-size-default">Noticias relacionadas</h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-3791910 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="3791910" data-element_type="section" wfd-id="110">
						<div class="elementor-container elementor-column-gap-default" wfd-id="111">
				<div class="elementor-row" wfd-id="112">
				<div class="elementor-element elementor-element-b47d674 elementor-column elementor-col-50 elementor-inner-column" data-id="b47d674" data-element_type="column" wfd-id="127">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="128">
					<div class="elementor-widget-wrap" wfd-id="129">
				<div class="elementor-element elementor-element-d129d0a elementor-widget elementor-widget-image" data-id="d129d0a" data-element_type="widget" data-widget_type="image.default" wfd-id="130">
				<div class="elementor-widget-container" wfd-id="131">
					<div class="elementor-image" wfd-id="132">
										<img width="169" height="199" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-ecc3731 elementor-column elementor-col-50 elementor-inner-column" data-id="ecc3731" data-element_type="column" wfd-id="113">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="114">
					<div class="elementor-widget-wrap" wfd-id="115">
				<div class="elementor-element elementor-element-05935c7 elementor-widget elementor-widget-text-editor" data-id="05935c7" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="119">
				<div class="elementor-widget-container" wfd-id="120">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="121"><p><span style="color: #00c0cd;" wfd-id="126">24 / 09 / 2019</span></p><p><span style="color: #00c0cd;" wfd-id="124"><strong><span style="text-decoration: underline;" wfd-id="125">ESPACIO PARA TÍTULO</span></strong></span><br><span style="color: #00c0cd;" wfd-id="122"><strong><span style="text-decoration: underline;" wfd-id="123">CORTO DE LA NOTICIA</span></strong></span></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-2eddd58 elementor-widget elementor-widget-text-editor" data-id="2eddd58" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="116">
				<div class="elementor-widget-container" wfd-id="117">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="118"><p>Categoría:<br>Comunicaciones</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-578d641 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="578d641" data-element_type="section" wfd-id="87">
						<div class="elementor-container elementor-column-gap-default" wfd-id="88">
				<div class="elementor-row" wfd-id="89">
				<div class="elementor-element elementor-element-ee28175 elementor-column elementor-col-50 elementor-inner-column" data-id="ee28175" data-element_type="column" wfd-id="104">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="105">
					<div class="elementor-widget-wrap" wfd-id="106">
				<div class="elementor-element elementor-element-3f7e850 elementor-widget elementor-widget-image" data-id="3f7e850" data-element_type="widget" data-widget_type="image.default" wfd-id="107">
				<div class="elementor-widget-container" wfd-id="108">
					<div class="elementor-image" wfd-id="109">
										<img width="169" height="199" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-fe8ff45 elementor-column elementor-col-50 elementor-inner-column" data-id="fe8ff45" data-element_type="column" wfd-id="90">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="91">
					<div class="elementor-widget-wrap" wfd-id="92">
				<div class="elementor-element elementor-element-ef33ae5 elementor-widget elementor-widget-text-editor" data-id="ef33ae5" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="96">
				<div class="elementor-widget-container" wfd-id="97">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="98"><p><span style="color: #00c0cd;" wfd-id="103">24 / 09 / 2019</span></p><p><span style="color: #00c0cd;" wfd-id="101"><strong><span style="text-decoration: underline;" wfd-id="102">ESPACIO PARA TÍTULO</span></strong></span><br><span style="color: #00c0cd;" wfd-id="99"><strong><span style="text-decoration: underline;" wfd-id="100">CORTO DE LA NOTICIA</span></strong></span></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-c8a52fb elementor-widget elementor-widget-text-editor" data-id="c8a52fb" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="93">
				<div class="elementor-widget-container" wfd-id="94">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="95"><p>Categoría:<br>Comunicaciones</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-6cc2af8 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="6cc2af8" data-element_type="section" wfd-id="64">
						<div class="elementor-container elementor-column-gap-default" wfd-id="65">
				<div class="elementor-row" wfd-id="66">
				<div class="elementor-element elementor-element-6ecb871 elementor-column elementor-col-50 elementor-inner-column" data-id="6ecb871" data-element_type="column" wfd-id="81">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="82">
					<div class="elementor-widget-wrap" wfd-id="83">
				<div class="elementor-element elementor-element-3f0e8c7 elementor-widget elementor-widget-image" data-id="3f0e8c7" data-element_type="widget" data-widget_type="image.default" wfd-id="84">
				<div class="elementor-widget-container" wfd-id="85">
					<div class="elementor-image" wfd-id="86">
										<img width="169" height="199" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-a215a3e elementor-column elementor-col-50 elementor-inner-column" data-id="a215a3e" data-element_type="column" wfd-id="67">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="68">
					<div class="elementor-widget-wrap" wfd-id="69">
				<div class="elementor-element elementor-element-e77b7ef elementor-widget elementor-widget-text-editor" data-id="e77b7ef" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="73">
				<div class="elementor-widget-container" wfd-id="74">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="75"><p><span style="color: #00c0cd;" wfd-id="80">24 / 09 / 2019</span></p><p><span style="color: #00c0cd;" wfd-id="78"><strong><span style="text-decoration: underline;" wfd-id="79">ESPACIO PARA TÍTULO</span></strong></span><br><span style="color: #00c0cd;" wfd-id="76"><strong><span style="text-decoration: underline;" wfd-id="77">CORTO DE LA NOTICIA</span></strong></span></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0c68689 elementor-widget elementor-widget-text-editor" data-id="0c68689" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="70">
				<div class="elementor-widget-container" wfd-id="71">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="72"><p>Categoría:<br>Comunicaciones</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
				<div class="post-tags" wfd-id="54">
					</div>
			</div>
        <?php endif;?>
        
         <?php if(get_post_type()=='page'):?> 
        <div>
            <?php the_content(); 
			echo'<p>';
			the_field('documentos');
		    echo '</p>';
		    ?>
        </div>
        <?php endif;?>
        
        <?php if(get_post_type()=='videos'):?> 
        <div>
            <div data-elementor-type="wp-post" data-elementor-id="2739" class="elementor elementor-2739" data-elementor-settings="[]" wfd-id="56">
			<div class="elementor-inner" wfd-id="57">
				<div class="elementor-section-wrap" wfd-id="58">
							
				<section class="elementor-element elementor-element-5a9c045 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="5a9c045" data-element_type="section" wfd-id="95">
						<div class="elementor-container elementor-column-gap-default" wfd-id="96">
				<div class="elementor-row" wfd-id="97">
				<div class="elementor-element elementor-element-22b274a elementor-column elementor-col-100 elementor-top-column" data-id="22b274a" data-element_type="column" wfd-id="98">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="99">
					<div class="elementor-widget-wrap" wfd-id="100">
				<section class="elementor-element elementor-element-ed8278f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="ed8278f" data-element_type="section" wfd-id="115">
						<div class="elementor-container elementor-column-gap-default" wfd-id="116">
				<div class="elementor-row" wfd-id="117">
				<div class="elementor-element elementor-element-dfda008 elementor-column elementor-col-50 elementor-inner-column" data-id="dfda008" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="123">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="124">
					<div class="elementor-widget-wrap" wfd-id="125">
				<div class="elementor-element elementor-element-699066b elementor-widget elementor-widget-spacer" data-id="699066b" data-element_type="widget" data-widget_type="spacer.default" wfd-id="126">
				<div class="elementor-widget-container" wfd-id="127">
					<div class="elementor-spacer" wfd-id="128">
			<div class="elementor-spacer-inner" wfd-id="129"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-040e396 elementor-column elementor-col-50 elementor-inner-column" data-id="040e396" data-element_type="column" wfd-id="118">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="119">
					<div class="elementor-widget-wrap" wfd-id="120">
				<div class="elementor-element elementor-element-0fef6ac elementor-widget elementor-widget-heading" data-id="0fef6ac" data-element_type="widget" data-widget_type="heading.default" wfd-id="121">
				<div class="elementor-widget-container" wfd-id="122">
			<h2 class="elementor-heading-title elementor-size-default"><?php the_title(); ?></h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				
						</div>
			</div>
		</div>
		
		</div>
		    <p><?php the_content();?></p>
        <div class="embed-container">
             <p><?php the_field('video'); ?></p>
        </div>
        </br>
        <?php endif;?>
		
		<div class="page-content">
	    <?php if(get_post_type()=='galerias'):?>
        <div class="page-content" wfd-id="53">
				<div data-elementor-type="wp-post" data-elementor-id="2822" class="elementor elementor-2822" data-elementor-settings="[]" wfd-id="55">
			<div class="elementor-inner" wfd-id="56">
				<div class="elementor-section-wrap" wfd-id="57">
							<section class="elementor-element elementor-element-906a10b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="906a10b" data-element_type="section" wfd-id="213">
						<div class="elementor-container elementor-column-gap-default" wfd-id="214">
				<div class="elementor-row" wfd-id="215">
				<div class="elementor-element elementor-element-d306bbc elementor-column elementor-col-100 elementor-top-column" data-id="d306bbc" data-element_type="column" wfd-id="216">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="217">
					<div class="elementor-widget-wrap" wfd-id="218">
				<div class="elementor-element elementor-element-9d7fadb elementor-widget elementor-widget-image" data-id="9d7fadb" data-element_type="widget" data-widget_type="image.default" wfd-id="219">
				<div class="elementor-widget-container" wfd-id="220">
					<div class="elementor-image" wfd-id="221">
										<?php the_post_thumbnail('full'); ?>											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-9e1db27 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="9e1db27" data-element_type="section" wfd-id="197">
						<div class="elementor-container elementor-column-gap-default" wfd-id="198">
				<div class="elementor-row" wfd-id="199">
				<div class="elementor-element elementor-element-fa58f5e elementor-column elementor-col-50 elementor-top-column" data-id="fa58f5e" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="206">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="207">
					<div class="elementor-widget-wrap" wfd-id="208">
				<div class="elementor-element elementor-element-cee9141 elementor-widget elementor-widget-spacer" data-id="cee9141" data-element_type="widget" data-widget_type="spacer.default" wfd-id="209">
				<div class="elementor-widget-container" wfd-id="210">
					<div class="elementor-spacer" wfd-id="211">
			<div class="elementor-spacer-inner" wfd-id="212"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-597c242 elementor-column elementor-col-50 elementor-top-column" data-id="597c242" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="200">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="201">
					<div class="elementor-widget-wrap" wfd-id="202">
				<div class="elementor-element elementor-element-a31df63 elementor-widget elementor-widget-text-editor" data-id="a31df63" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="203">
				<div class="elementor-widget-container" wfd-id="204">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="205"><p><?php the_title(); ?></p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-867426d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="867426d" data-element_type="section" wfd-id="162">
						<div class="elementor-container elementor-column-gap-default" wfd-id="163">
				<div class="elementor-row" wfd-id="164">
				<div class="elementor-element elementor-element-75bdc3c elementor-column elementor-col-25 elementor-top-column" data-id="75bdc3c" data-element_type="column" wfd-id="194">
			<div class="elementor-column-wrap" wfd-id="195">
					<div class="elementor-widget-wrap" wfd-id="196">
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-b73ec36 elementor-column elementor-col-25 elementor-top-column" data-id="b73ec36" data-element_type="column" wfd-id="175">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="176">
					<div class="elementor-widget-wrap" wfd-id="177">
				<div class="elementor-element elementor-element-bb1de12 elementor-absolute elementor-widget elementor-widget-spacer" data-id="bb1de12" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="spacer.default" wfd-id="182">
				<div class="elementor-widget-container" wfd-id="183">
					<div class="elementor-spacer" wfd-id="184">
			<div class="elementor-spacer-inner" wfd-id="185"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-8eef186 elementor-absolute elementor-widget elementor-widget-spacer" data-id="8eef186" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="spacer.default" wfd-id="178">
				<div class="elementor-widget-container" wfd-id="179">
					<div class="elementor-spacer" wfd-id="180">
			<div class="elementor-spacer-inner" wfd-id="181"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-7ffb217 elementor-column elementor-col-25 elementor-top-column" data-id="7ffb217" data-element_type="column" wfd-id="168">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="169">
					<div class="elementor-widget-wrap" wfd-id="170">
				<div class="elementor-element elementor-element-e62ef4b elementor-absolute elementor-widget elementor-widget-spacer" data-id="e62ef4b" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;absolute&quot;}" data-widget_type="spacer.default" wfd-id="171">
				<div class="elementor-widget-container" wfd-id="172">
					<div class="elementor-spacer" wfd-id="173">
			<div class="elementor-spacer-inner" wfd-id="174"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-914cdcb elementor-column elementor-col-25 elementor-top-column" data-id="914cdcb" data-element_type="column" wfd-id="165">
			<div class="elementor-column-wrap" wfd-id="166">
					<div class="elementor-widget-wrap" wfd-id="167">
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-334cc68 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="334cc68" data-element_type="section" wfd-id="58">
						<div class="elementor-container elementor-column-gap-default" wfd-id="59">
				<div class="elementor-row" wfd-id="60">
				<div class="elementor-element elementor-element-edb2b10 elementor-column elementor-col-50 elementor-top-column" data-id="edb2b10" data-element_type="column" wfd-id="147">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="148">
					<div class="elementor-widget-wrap" wfd-id="149">
				<div class="elementor-element elementor-element-b39ca31 elementor-widget elementor-widget-text-editor" data-id="b39ca31" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="159">
				<div class="elementor-widget-container" wfd-id="160">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="161"><p>Publicado por: <?php the_author(); ?> Fecha: <?php the_date(); ?></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-16f1ade elementor-widget elementor-widget-text-editor" data-id="16f1ade" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="156">
				<div class="elementor-widget-container" wfd-id="157">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="158"><p><?php the_content(); ?></p>
					<?php
$images = get_field('galeria');
if( $images ):
?>
<div id="slider" class="flexslider">
        <ul class="slides">
            <?php foreach( $images as $image ): ?>
                <li>
                <a href="<?php echo esc_url($image['url']); ?>">
                     <img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                </a>
                <p><?php echo esc_html($image['caption']); ?></p>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; 
?>
					</div>
				</div>
				</div>
				
				<div class="elementor-element elementor-element-48ab0f4 elementor-widget elementor-widget-image" data-id="48ab0f4" data-element_type="widget" data-widget_type="image.default" wfd-id="150">
				<div class="elementor-widget-container" wfd-id="151">
					<div class="elementor-image" wfd-id="152">
										<img width="800" height="453" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-1024x580.png" class="attachment-large size-large" alt="" srcset="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-1024x580.png 1024w, http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-300x170.png 300w, http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande-768x435.png 768w, http://158.69.170.226/~intranet/wp-content/uploads/2019/10/demo-GaleríaInternaGrande.png 1858w" sizes="(max-width: 800px) 100vw, 800px">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-9e92613 elementor-column elementor-col-50 elementor-top-column" data-id="9e92613" data-element_type="column" wfd-id="61">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="62">
					<div class="elementor-widget-wrap" wfd-id="63">
				<section class="elementor-element elementor-element-cf0aaee elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="cf0aaee" data-element_type="section" wfd-id="133">
						<div class="elementor-container elementor-column-gap-default" wfd-id="134">
				<div class="elementor-row" wfd-id="135">
				<div class="elementor-element elementor-element-163befc elementor-column elementor-col-50 elementor-inner-column" data-id="163befc" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="141">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="142">
					<div class="elementor-widget-wrap" wfd-id="143">
				<div class="elementor-element elementor-element-3f5ec5c elementor-widget elementor-widget-image" data-id="3f5ec5c" data-element_type="widget" data-widget_type="image.default" wfd-id="144">
				<div class="elementor-widget-container" wfd-id="145">
					<div class="elementor-image" wfd-id="146">
										<img width="47" height="42" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/11/ico-consultasfrecuentes.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-1f45746 elementor-column elementor-col-50 elementor-inner-column" data-id="1f45746" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" wfd-id="136">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="137">
					<div class="elementor-widget-wrap" wfd-id="138">
				<div class="elementor-element elementor-element-b711472 elementor-widget elementor-widget-heading" data-id="b711472" data-element_type="widget" data-widget_type="heading.default" wfd-id="139">
				<div class="elementor-widget-container" wfd-id="140">
			<h2 class="elementor-heading-title elementor-size-default">Galerias relacionadas</h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-3791910 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="3791910" data-element_type="section" wfd-id="110">
						<div class="elementor-container elementor-column-gap-default" wfd-id="111">
				<div class="elementor-row" wfd-id="112">
				<div class="elementor-element elementor-element-b47d674 elementor-column elementor-col-50 elementor-inner-column" data-id="b47d674" data-element_type="column" wfd-id="127">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="128">
					<div class="elementor-widget-wrap" wfd-id="129">
				<div class="elementor-element elementor-element-d129d0a elementor-widget elementor-widget-image" data-id="d129d0a" data-element_type="widget" data-widget_type="image.default" wfd-id="130">
				<div class="elementor-widget-container" wfd-id="131">
					<div class="elementor-image" wfd-id="132">
										<img width="169" height="199" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-ecc3731 elementor-column elementor-col-50 elementor-inner-column" data-id="ecc3731" data-element_type="column" wfd-id="113">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="114">
					<div class="elementor-widget-wrap" wfd-id="115">
				<div class="elementor-element elementor-element-05935c7 elementor-widget elementor-widget-text-editor" data-id="05935c7" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="119">
				<div class="elementor-widget-container" wfd-id="120">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="121"><p><span style="color: #00c0cd;" wfd-id="126">24 / 09 / 2019</span></p><p><span style="color: #00c0cd;" wfd-id="124"><strong><span style="text-decoration: underline;" wfd-id="125">ESPACIO PARA TÍTULO</span></strong></span><br><span style="color: #00c0cd;" wfd-id="122"><strong><span style="text-decoration: underline;" wfd-id="123">CORTO DE LA NOTICIA</span></strong></span></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-2eddd58 elementor-widget elementor-widget-text-editor" data-id="2eddd58" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="116">
				<div class="elementor-widget-container" wfd-id="117">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="118"><p>Categoría:<br>Comunicaciones</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-578d641 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="578d641" data-element_type="section" wfd-id="87">
						<div class="elementor-container elementor-column-gap-default" wfd-id="88">
				<div class="elementor-row" wfd-id="89">
				<div class="elementor-element elementor-element-ee28175 elementor-column elementor-col-50 elementor-inner-column" data-id="ee28175" data-element_type="column" wfd-id="104">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="105">
					<div class="elementor-widget-wrap" wfd-id="106">
				<div class="elementor-element elementor-element-3f7e850 elementor-widget elementor-widget-image" data-id="3f7e850" data-element_type="widget" data-widget_type="image.default" wfd-id="107">
				<div class="elementor-widget-container" wfd-id="108">
					<div class="elementor-image" wfd-id="109">
										<img width="169" height="199" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-fe8ff45 elementor-column elementor-col-50 elementor-inner-column" data-id="fe8ff45" data-element_type="column" wfd-id="90">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="91">
					<div class="elementor-widget-wrap" wfd-id="92">
				<div class="elementor-element elementor-element-ef33ae5 elementor-widget elementor-widget-text-editor" data-id="ef33ae5" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="96">
				<div class="elementor-widget-container" wfd-id="97">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="98"><p><span style="color: #00c0cd;" wfd-id="103">24 / 09 / 2019</span></p><p><span style="color: #00c0cd;" wfd-id="101"><strong><span style="text-decoration: underline;" wfd-id="102">ESPACIO PARA TÍTULO</span></strong></span><br><span style="color: #00c0cd;" wfd-id="99"><strong><span style="text-decoration: underline;" wfd-id="100">CORTO DE LA NOTICIA</span></strong></span></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-c8a52fb elementor-widget elementor-widget-text-editor" data-id="c8a52fb" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="93">
				<div class="elementor-widget-container" wfd-id="94">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="95"><p>Categoría:<br>Comunicaciones</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-6cc2af8 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="6cc2af8" data-element_type="section" wfd-id="64">
						<div class="elementor-container elementor-column-gap-default" wfd-id="65">
				<div class="elementor-row" wfd-id="66">
				<div class="elementor-element elementor-element-6ecb871 elementor-column elementor-col-50 elementor-inner-column" data-id="6ecb871" data-element_type="column" wfd-id="81">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="82">
					<div class="elementor-widget-wrap" wfd-id="83">
				<div class="elementor-element elementor-element-3f0e8c7 elementor-widget elementor-widget-image" data-id="3f0e8c7" data-element_type="widget" data-widget_type="image.default" wfd-id="84">
				<div class="elementor-widget-container" wfd-id="85">
					<div class="elementor-image" wfd-id="86">
										<img width="169" height="199" src="http://158.69.170.226/~intranet/wp-content/uploads/2019/10/mini-NoticiasRelacionadas.png" class="attachment-large size-large" alt="">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-a215a3e elementor-column elementor-col-50 elementor-inner-column" data-id="a215a3e" data-element_type="column" wfd-id="67">
			<div class="elementor-column-wrap  elementor-element-populated" wfd-id="68">
					<div class="elementor-widget-wrap" wfd-id="69">
				<div class="elementor-element elementor-element-e77b7ef elementor-widget elementor-widget-text-editor" data-id="e77b7ef" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="73">
				<div class="elementor-widget-container" wfd-id="74">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="75"><p><span style="color: #00c0cd;" wfd-id="80">24 / 09 / 2019</span></p><p><span style="color: #00c0cd;" wfd-id="78"><strong><span style="text-decoration: underline;" wfd-id="79">ESPACIO PARA TÍTULO</span></strong></span><br><span style="color: #00c0cd;" wfd-id="76"><strong><span style="text-decoration: underline;" wfd-id="77">CORTO DE LA NOTICIA</span></strong></span></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0c68689 elementor-widget elementor-widget-text-editor" data-id="0c68689" data-element_type="widget" data-widget_type="text-editor.default" wfd-id="70">
				<div class="elementor-widget-container" wfd-id="71">
					<div class="elementor-text-editor elementor-clearfix" wfd-id="72"><p>Categoría:<br>Comunicaciones</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
				<div class="post-tags" wfd-id="54">
					</div>
			</div>
        <?php endif;?>
		
		<div class="post-tags">
			<?php the_tags( '<span class="tag-links">' . __( 'Tagged ', 'hello-elementor' ), null, '</span>' ); ?>
		</div>
		<?php wp_link_pages(); ?>
	</div>

	<?php comments_template(); ?>
</main>

	<?php
endwhile;
