<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'HELLO_ELEMENTOR_VERSION', '2.2.0' );

if ( ! isset( $content_width ) ) {
	$content_width = 800; // Pixels.
}

if ( ! function_exists( 'hello_elementor_setup' ) ) {
	/**
	 * Set up theme support.
	 *
	 * @return void
	 */
	function hello_elementor_setup() {
		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_load_textdomain', [ true ], '2.0', 'hello_elementor_load_textdomain' );
		if ( apply_filters( 'hello_elementor_load_textdomain', $hook_result ) ) {
			load_theme_textdomain( 'hello-elementor', get_template_directory() . '/languages' );
		}

		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_menus', [ true ], '2.0', 'hello_elementor_register_menus' );
		if ( apply_filters( 'hello_elementor_register_menus', $hook_result ) ) {
			register_nav_menus( array( 'menu-1' => __( 'Primary', 'hello-elementor' ) ) );
		}

		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_theme_support', [ true ], '2.0', 'hello_elementor_add_theme_support' );
		if ( apply_filters( 'hello_elementor_add_theme_support', $hook_result ) ) {
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'title-tag' );
			add_theme_support(
				'html5',
				array(
					'search-form',
					'comment-form',
					'comment-list',
					'gallery',
					'caption',
				)
			);
			add_theme_support(
				'custom-logo',
				array(
					'height'      => 100,
					'width'       => 350,
					'flex-height' => true,
					'flex-width'  => true,
				)
			);

			/*
			 * Editor Style.
			 */
			add_editor_style( 'editor-style.css' );

			/*
			 * WooCommerce.
			 */
			$hook_result = apply_filters_deprecated( 'elementor_hello_theme_add_woocommerce_support', [ true ], '2.0', 'hello_elementor_add_woocommerce_support' );
			if ( apply_filters( 'hello_elementor_add_woocommerce_support', $hook_result ) ) {
				// WooCommerce in general.
				add_theme_support( 'woocommerce' );
				// Enabling WooCommerce product gallery features (are off by default since WC 3.0.0).
				// zoom.
				add_theme_support( 'wc-product-gallery-zoom' );
				// lightbox.
				add_theme_support( 'wc-product-gallery-lightbox' );
				// swipe.
				add_theme_support( 'wc-product-gallery-slider' );
			}
		}
	}
}
add_action( 'after_setup_theme', 'hello_elementor_setup' );

if ( ! function_exists( 'hello_elementor_scripts_styles' ) ) {
	function hello_elementor_scripts_styles() {
		$enqueue_basic_style = apply_filters_deprecated( 'elementor_hello_theme_enqueue_style', [ true ], '2.0', 'hello_elementor_enqueue_style' );
		$min_suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		if ( apply_filters( 'hello_elementor_enqueue_style', $enqueue_basic_style ) ) {
			wp_enqueue_style( 'estilos-personalizados', get_template_directory_uri() . '/estilos-personalizados.css',false,'1.1','all');
			wp_enqueue_script( 'script', get_template_directory_uri() . '/script.js', array ( 'jquery' ), 1.1, true);
			wp_enqueue_style(
				'hello-elementor',
				get_template_directory_uri() . '/style' . $min_suffix . '.css',
				[],
				HELLO_ELEMENTOR_VERSION
			);
		}

		if ( apply_filters( 'hello_elementor_enqueue_theme_style', true ) ) {
			wp_enqueue_style(
				'hello-elementor-theme-style',
				get_template_directory_uri() . '/theme' . $min_suffix . '.css',
				[],
				HELLO_ELEMENTOR_VERSION
			);
		}
	}
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_scripts_styles' );
if ( ! function_exists( 'hello_elementor_register_elementor_locations' ) ) {
	/**
	 * Register Elementor Locations.
	 *
	 * @param ElementorPro\Modules\ThemeBuilder\Classes\Locations_Manager $elementor_theme_manager theme manager.
	 *
	 * @return void
	 */
	function hello_elementor_register_elementor_locations( $elementor_theme_manager ) {
		$hook_result = apply_filters_deprecated( 'elementor_hello_theme_register_elementor_locations', [ true ], '2.0', 'hello_elementor_register_elementor_locations' );
		if ( apply_filters( 'hello_elementor_register_elementor_locations', $hook_result ) ) {
			$elementor_theme_manager->register_all_core_location();
		}
	}
}
add_action( 'elementor/theme/register_locations', 'hello_elementor_register_elementor_locations' );

if ( ! function_exists( 'hello_elementor_content_width' ) ) {
	/**
	 * Set default content width.
	 *
	 * @return void
	 */
	function hello_elementor_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'hello_elementor_content_width', 800 );
	}
}
add_action( 'after_setup_theme', 'hello_elementor_content_width', 0 );

if ( is_admin() ) {
	require get_template_directory() . '/includes/admin-functions.php';
}

if ( ! function_exists( 'hello_elementor_check_hide_title' ) ) {
	/**
	 * Check hide title.
	 *
	 * @param bool $val default value.
	 *
	 * @return bool
	 */
	function hello_elementor_check_hide_title( $val ) {
		if ( defined( 'ELEMENTOR_VERSION' ) ) {
			$current_doc = \Elementor\Plugin::instance()->documents->get( get_the_ID() );
			if ( $current_doc && 'yes' === $current_doc->get_settings( 'hide_title' ) ) {
				$val = false;
			}
		}
		return $val;
	}
}
add_filter( 'hello_elementor_page_title', 'hello_elementor_check_hide_title' );

/**
 * Wrapper function to deal with backwards compatibility.
 */
if ( ! function_exists( 'hello_elementor_body_open' ) ) {
	function hello_elementor_body_open() {
		if ( function_exists( 'wp_body_open' ) ) {
			wp_body_open();
		} else {
			do_action( 'wp_body_open' );
		}
	}
}
add_filter( 'facetwp_pager_html', function( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];

    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page izqq" data-page="' . ($page - 1) . '"><i class="eicon-chevron-left" aria-hidden="true"></i></a>';
    }
    else  {
        $output .= '<a class="facetwp-page izqq"><i class="eicon-chevron-left" aria-hidden="true"></i></a>';
    }
    if ( 1 < $params['total_pages'] ) {
        for ( $i = 1; $i <= $params['total_pages']; $i++ ) {
            $is_curr = ( $i === $params['page'] ) ? ' active' : '';
            $output .= '<a class="facetwp-page' . $is_curr . '" data-page="' . $i . '">' . $i . '</a>';
        }
    }
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page derr" data-page="' . ($page + 1) . '"><i class="eicon-chevron-right" aria-hidden="true"></i></a>';
    }
    else {
        $output .= '<a class="facetwp-page derr"><i class="eicon-chevron-right" aria-hidden="true"></i></a>';
    }

    return $output;
}, 10, 2 );

//add_filter( 'facetwp_shortcode_html', function( $output, $atts) {
	//if ( $atts['template'] = 'sala_de_prensa' ) { // replace 'example' with name of your template
    /** modify replacement as needed, make sure you keep the facetwp-template class **/
	//	$output = str_replace( 'class="facetwp-template"', 'id="masonry-container" class="facetwp-template small-up-1 medium-up-2 large-up-3"', $output );
	//}
	//return $output; 
//}, 10, 2 );

add_filter( 'facetwp_facet_html', function( $output, $params ) {
	if ( 'fechas_publicaciones' == $params['facet']['name'] ) { // change 'my_date_facet' to the name of your date picker facet
		$output = str_replace( 'placeholder="Fecha de inicio"', 'placeholder="DD/MM/AAAA"', $output );
		$output = str_replace( 'placeholder="Fecha de finalización"', 'placeholder="DD/MM/AAAA"', $output );
	}
	return $output;
}, 10, 2 );

add_filter( 'facetwp_builder_item_value', function( $value, $item ) {
    if ( 'el-aq5g2n' == $item['settings']['name'] ) {
		if ( is_array( $value ) && ! empty( $value['url'] ) ) {
			$src = $value['url'];
			$value = '<div class="contenedor-podcast">';
			$value .= '<audio controls>';
			$value .= '<source src="' . $src . '" type="audio/mpeg">';
			$value .= 'Your browser does not support the audio element.';
			$value .= '</audio>';
			$value .= '</div>';
			
		}
    }
    return $value;
}, 10, 2 );



add_filter( 'facetwp_builder_item_value', function( $value, $item ) {
    if ( 'el-00q09u' == $item['settings']['name'] ) {
		if ( 'post_content' == $item['source'] ) {
        $value = substr( $value, 0, 100 );
        }
    }
    return $value;
}, 10, 2 );

function fwp_disable_auto_refresh() {
?>
<script>
(function($) {
    $(function() {
        if ('undefined' !== typeof FWP) {
            FWP.auto_refresh = false;
        }
    });
})(jQuery);
</script>
<?php
}
add_action( 'wp_head', 'fwp_disable_auto_refresh', 100 );
add_action('wp_enqueue_scripts', 'register_content_scripts');   
function register_content_scripts(){
//wp_register_style( 'flexslider-custom-css', get_stylesheet_directory_uri() . '/css/flexslider.css' );
wp_enqueue_style( 'flexslider-custom-css', get_template_directory_uri() . '/css/flexslider.css',false,'1.1','all');
wp_enqueue_script( 'acf-flexslider-scripts', get_template_directory_uri() . '/jquery.flexslider-min.js', array ( 'jquery' ), 1.1, true);
//wp_enqueue_script( 'flexslider-init', get_template_directory_uri() . '/flex-init.js', array ( 'jquery' ), 1.1, true);
//wp_register_script( 'acf-flexslider-scripts',  get_stylesheet_directory_uri() . '/js/jquery.flexslider-min.js',  array( 'jquery' ), 1, 1 );
//wp_register_script( 'flexslider-init',  get_stylesheet_directory_uri() . '/js/flex-init.js',  array( 'jquery' ), 1, 1 );
}

