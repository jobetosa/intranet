<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/home4/intranet/public_html/wp-content/plugins/wp-super-cache/' );
define( 'DB_NAME', 'intranet_bd' );

/** MySQL database username */
define( 'DB_USER', 'intranet_user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Xhw(Gpn.&6GL' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'vo4uqpby2y00gsdyh6smof43xh1nlumcwxhyoxptpnkyogpc4ugbneotk9tdpqwu' );
define( 'SECURE_AUTH_KEY',  'bfvcbzbml6pto59hnatcye99fvmowtsxeuflln17tmycbxyvmpholzkprrujstrt' );
define( 'LOGGED_IN_KEY',    'feuiunag6sqbnnx4a9e5pwhqqzrjrsyrw4784yvh7nzf2wr1xwstsdcg2yz46ali' );
define( 'NONCE_KEY',        'rtwhq0sfesujstmwd4jwwkcojtthbciek9xbvj7hwmpbdmtxnoynxmqgdwgfgdaf' );
define( 'AUTH_SALT',        'pdeapdbckzd0peyf99www00qaqprydmy3pmnpkcdhb3akh9jp4jdce1zfvd7t3fn' );
define( 'SECURE_AUTH_SALT', 'qwdg7rijr13r3ahkjtv1mivaholvig7hxprjdpnyqevygxpy3g5sevubrtzl7ivy' );
define( 'LOGGED_IN_SALT',   'kregixofpk3kppvpexag3c0layjm08msyscbp4drg2vhopcegbbtcexrh1pll8ax' );
define( 'NONCE_SALT',       'yakixr1s28toymoqjtmrtli0yxjwy3qdlbnsoszhsl8itbnsceqtrdawueszexxi' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpy1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define('WP_MEMORY_LIMIT', '512M');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
